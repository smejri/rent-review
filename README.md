# RentReview

Renting cars, houses, equipment, etc always goes under the radar, there is no indication about the product and the service. Rent Review is the web application that will help users (both customers and owners) make rent related decision. 

#### [Beta version](https://rentreviewwebapp.azurewebsites.net/) is available.

## Tech

RentReview uses a number of open source projects to work properly:

* [ASP.NET MVC](https://www.asp.net/)
* [Unobtrusive Javascript](https://en.wikipedia.org/wiki/Unobtrusive_JavaScript)
* [jQuery](https://jquery.com/)
* CSS3
* [Bootstrap](https://getbootstrap.com/)
* [Razor Engine](https://en.wikipedia.org/wiki/ASP.NET_Razor)


## Todos

 - Implement notification with [SignalR](https://www.asp.net/signalr)
 - Analyze and improve caching
 - Consider using [AngularJs](https://angularjs.org/) or [React](https://reactjs.org/) to improve performance
 - Consider migrating to [ASP.NET Core](https://docs.microsoft.com/en-us/aspnet/core/?view=aspnetcore-2.1)

License
----
RentReview is under [No License](https://choosealicense.com/no-permission/): nobody can use, copy, distribute, or modify this work without being at risk of take-downs, shake-downs, or litigation.


