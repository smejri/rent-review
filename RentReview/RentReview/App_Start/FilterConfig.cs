﻿using RentReview.CustomFilters;
using System.Web.Mvc;

namespace RentReview
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
            filters.Add(new RequireHttpsAttribute());
            // filters.Add(new ProfileVerificationAttribute());
        }
    }
}
