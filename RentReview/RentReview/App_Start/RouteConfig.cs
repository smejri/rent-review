﻿using System.Web.Mvc;
using System.Web.Routing;

namespace RentReview
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "ProfileSearch",
                url: "Profile/Search/{searchTerm}",
                defaults: new { controller = "Profile", action = "Search"}
            );

            routes.MapRoute(
                name: "Inventory",
                url: "Inventory/{action}/{UserId}",
                defaults: new {controller = "Inventory", action = "Index", UserId = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "Profile",
                url: "Profile/{action}/{UserId}",
                defaults: new { controller = "Profile", action = "Index", UserId = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
