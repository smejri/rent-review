﻿namespace RentReview.Controllers
{
    using Microsoft.AspNet.Identity;
    using RentReview.Models;
    using System.IO;
    using System.Linq;
    using System.Security.Principal;
    using System.Web.Mvc;

    public class BaseController : Controller
    {
        private RentReviewDb db;

        protected RentReviewDb Db
        {
            get
            {
                if (this.db == null)
                {
                    this.db = new RentReviewDb();
                }

                return this.db;
            }
        }

        protected IIdentity CurrentUserIdentity
        {
            get
            {
                return System.Web.HttpContext.Current.User.Identity;
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (this.db != null)
            {
                this.Db.Dispose();
            }

            base.Dispose(disposing);
        }

        [NonAction]
        protected internal UserProfile GetCurrentUserProfile()
        {
            string currentUserId = this.CurrentUserIdentity.GetUserId();
            return this.Db.Profiles.Where(profile => profile.UserId == currentUserId).FirstOrDefault();
        }

        [NonAction]
        internal protected UserStatus VerifyOwnership(string UserId = null)
        {
            string currentUserId = this.CurrentUserIdentity.GetUserId();
            UserProfile requestedUserProfile = this.Db.Profiles.Where(p => p.UserId == UserId).FirstOrDefault();

            // When the UserId is null, we assume it's the current active user
            if (string.IsNullOrEmpty(UserId) || UserId.Equals(currentUserId))
            {
                ViewBag.IsOwner = true;
                return UserStatus.Current;
            }
            // Requesting someone else data and it exists in the system
            else if (UserId != currentUserId && requestedUserProfile != null)
            {
                ViewBag.IsOwner = false;
                ViewBag.Username = requestedUserProfile.Name;
                return UserStatus.Stranger;
            }

            return UserStatus.Undefined;
        }

        [NonAction]
        protected internal string RenderViewAsString(string viewName, object model)
        {
            if (string.IsNullOrEmpty(viewName))
            {
                viewName = ControllerContext.RouteData.GetRequiredString("action");
            }

            ViewData.Model = model;

            using (StringWriter sw = new StringWriter())
            {
                ViewEngineResult viewResult = ViewEngines.Engines.FindPartialView(ControllerContext, viewName);
                ViewContext viewContext = new ViewContext(ControllerContext, viewResult.View, ViewData, TempData, sw);
                viewResult.View.Render(viewContext, sw);

                return sw.GetStringBuilder().ToString();
            }
        }
    }
}