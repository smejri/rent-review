﻿namespace RentReview.Controllers
{
    using Microsoft.AspNet.Identity;
    using RentReview.CustomFilters;
    using RentReview.Models;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using System.Net;
    using System.Web.Mvc;

    [Authorize]
    public class ContractController : BaseController
    {
        // GET: RentContract
        public ActionResult Index(string UserId = null)
        {
            switch (this.VerifyOwnership(UserId))
            {
                case UserStatus.Current:
                    UserId = this.CurrentUserIdentity.GetUserId();
                    break;
                case UserStatus.Stranger:
                    ViewBag.Username = this.Db.Profiles.Where(profile => profile.UserId == UserId).FirstOrDefault().Name;
                    break;
                case UserStatus.Undefined:
                    return new HttpNotFoundResult("Invalid User");
            }

            IEnumerable<ContractViewModel> associatedContracts = this.Db.Contracts.Where(contract => contract.Item.Owner.UserId == UserId ||
                                                                                                contract.Customer.UserId == UserId)
                                                                                                .Select(contract => new ContractViewModel { Contract = contract});
            return View("ContractList", associatedContracts);
        }

        [ProfileVerification]
        public ActionResult DetailedContract(int id)
        {
            
            RentContract contract = this.Db.Contracts.Find(id);

            if(contract == null)
            {
                return new HttpNotFoundResult($"Contract with id {id} does not exist.");
            }

            string UserId = this.CurrentUserIdentity.GetUserId();

            if (UserId != contract.Customer.UserId && UserId != contract.Item.Owner.UserId)
            {
                return new HttpUnauthorizedResult($"You are unothorized to view contract with id {id}.");
            }
            
            return View("DetailedContract", contract);
        }

        // GET: RentContract/Create
        [ProfileVerification]
        public ActionResult Create()
        {
            return View();
        }

        [ProfileVerification]
        public ActionResult Rent(int itemId)
        {
            RentContract contract = new RentContract();
            contract.Item = this.Db.Inventory.Find(itemId);

            if (contract.Item == null)
            {
                return new HttpNotFoundResult($"Cound not find an inventory item with id {itemId}");
            }

            UserProfile currentUser = this.GetCurrentUserProfile();

            if (contract.Item.Owner.UserId != currentUser.UserId)
            {
                contract.Customer = currentUser;
            }

            return View("Create", contract);
        }

        // POST: RentContract/Create
        [HttpPost]
        [ProfileVerification]
        public ActionResult Create(RentContract contract, string customerId, int itemId)
        {
            try
            {
                if (ModelState.IsValid && contract != null)
                {
                    contract.Item = this.Db.Inventory.Find(itemId);

                    if (contract.Item == null)
                    {
                        return new HttpNotFoundResult($"Cound not find an inventory item with id {itemId}");
                    }

                    contract.Customer = this.Db.Profiles.Find(customerId);
                    string errorMessage = this.ValidateContractCounterparts(contract);

                    if (!string.IsNullOrEmpty(errorMessage))
                    {
                        return new HttpStatusCodeResult(HttpStatusCode.BadRequest, errorMessage);
                    }

                    this.HandleUserResponse(contract, UserResponse.Approved);

                    this.Db.Contracts.Add(contract);
                    this.Db.SaveChanges();
                }

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        [HttpPost]
        [ProfileVerification]
        public ActionResult Approve(int id)
        {
            try
            {
                RentContract contract = this.Db.Contracts.Find(id);

                if (contract != null)
                {
                    this.HandleUserResponse(contract, UserResponse.Approved);

                    this.Db.Entry(contract).State = EntityState.Modified;
                    this.Db.SaveChanges();

                    if (Request.IsAjaxRequest())
                    {
                        return PartialView("Response", new UserResponseViewModel
                        {
                            Response = UserResponse.Approved,
                            ContractId = id,
                            UserId = this.CurrentUserIdentity.GetUserId()
                        });
                    }
                    else
                    {
                        return RedirectToAction("Index");
                    }
                }
                else
                {
                    return new HttpNotFoundResult("No contract is associated iwth the given Id.");
                }
            }
            catch (Exception exp)
            {
                return new HttpNotFoundResult(exp.Message);
            }
        }

        [HttpPost]
        [ProfileVerification]
        public ActionResult Decline(int id)
        {
            try
            {
                RentContract contract = this.Db.Contracts.Find(id);

                if (contract != null)
                {
                    this.HandleUserResponse(contract, UserResponse.Declined);

                    this.Db.Entry(contract).State = EntityState.Modified;
                    this.Db.SaveChanges();
                    if (Request.IsAjaxRequest())
                    {
                        return PartialView("Response", new UserResponseViewModel
                        {
                            Response = UserResponse.Declined,
                            ContractId = id,
                            UserId = this.CurrentUserIdentity.GetUserId()
                        });
                    }
                    else
                    {
                        return RedirectToAction("Index");
                    }
                }
                else
                {
                    return new HttpNotFoundResult("No contract is associated iwth the given Id.");
                }
            }
            catch (Exception exp)
            {
                return new HttpNotFoundResult(exp.Message);
            }
        }

        // GET: RentContract/Edit/5
        [ProfileVerification]
        public ActionResult Revise(int id)
        {
            return View(this.Db.Contracts.Find(id));
        }

        // POST: RentContract/Edit/5
        [HttpPost]
        [ProfileVerification]
        public ActionResult Revise(int id, RentContract contract, string customerId, int itemId)
        {
            try
            {
                if (ModelState.IsValid && contract != null)
                {
                    contract.Customer = this.Db.Profiles.Find(customerId);
                    contract.Item = this.Db.Inventory.Find(itemId);

                    string errorMessage = this.ValidateContractCounterparts(contract);

                    if (!string.IsNullOrEmpty(errorMessage))
                    {
                        return new HttpStatusCodeResult(HttpStatusCode.BadRequest, errorMessage);
                    }

                    this.HandleUserResponse(contract, UserResponse.Approved, true);
                    
                    this.Db.Entry(contract).State = EntityState.Modified;
                    this.Db.SaveChanges();
                }

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
        
        [HttpPost]
        [ProfileVerification]
        public ActionResult Conclude(int id)
        {
            try
            {
                RentContract contract = this.Db.Contracts.Find(id);

                if (contract != null)
                {
                    contract.Terminated = true;

                    this.Db.Entry(contract).State = EntityState.Modified;
                    this.Db.SaveChanges();
                }
                else
                {
                    return new HttpNotFoundResult($"There is no contract with id {id}.");
                }

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        [HttpGet]
        public ActionResult GetContractCount(string UserId = null)
        {
            if (string.IsNullOrEmpty(UserId))
            {
                return Content("<p class='alert-danger>invalid User ID.</p>'");
            }

            int waitingApproval = this.Db.Contracts.Where(contract => (contract.Customer.UserId == UserId &&
                                                        contract.CustomerResponse == UserResponse.WaitingReponse) ||
                                                        (contract.Item.Owner.UserId == UserId &&
                                                        contract.OwnerResponse == UserResponse.WaitingReponse))
                                                .Count();

            int waitingReview = this.Db.Contracts.Where(contract => (contract.Customer.UserId == UserId &&
                                                        contract.OwnerReview != null &&
                                                        contract.CustomerReview ==null) ||
                                                        (contract.Item.Owner.UserId == UserId &&
                                                        contract.OwnerReview == null &&
                                                        contract.CustomerReview != null))
                                                .Count();

            return (Content($"<p>{waitingApproval} contract(s) waiting for approval.<br>{waitingReview} contract(s) waiting for review.</p>"));
        }

        [NonAction]
        public void HandleUserResponse(RentContract contract, UserResponse response, bool resetCounterpartResponse = false)
        {
            if (this.CurrentUserIdentity.GetUserId().Equals(contract.Customer.UserId))
            {
                contract.CustomerResponse = response;

                if(resetCounterpartResponse)
                {
                    contract.OwnerResponse = UserResponse.WaitingReponse;
                }
            }

            if (this.CurrentUserIdentity.GetUserId().Equals(contract.Item.Owner.UserId))
            {
                contract.OwnerResponse = response;

                if (resetCounterpartResponse)
                {
                    contract.CustomerResponse = UserResponse.WaitingReponse;
                }
            }
        }

        [NonAction]
        public string ValidateContractCounterparts(RentContract contract)
        {
            string ownerId = contract.Item != null ? contract.Item.Owner.UserId : null;
            string customerId = contract.Customer != null ? contract.Customer.UserId : null;

            if (customerId == null)
            {
                return "Customer must be defined!";
            }

            if (ownerId == null)
            {
                return "Owner must be deifned!";
            }

            if (customerId.Equals(ownerId))
            {
                return "Customer and owner cannot be the same person!";
            }

            if (customerId != this.CurrentUserIdentity.GetUserId() && ownerId != this.CurrentUserIdentity.GetUserId())
            {
                return "One of the counterparts must be you!";
            }

            return null;
        }
    }
}
