﻿namespace RentReview.Controllers
{
    using Microsoft.AspNet.Identity;
    using RentReview.CustomFilters;
    using RentReview.Models;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using System.Web.Mvc;

    [Authorize]
    public class InventoryController : BaseController
    {
        // GET: RentItems
        public ActionResult Index(string UserId)
        {
            switch (this.VerifyOwnership(UserId))
            {
                case UserStatus.Current:
                    UserId = this.CurrentUserIdentity.GetUserId();
                    break;
                case UserStatus.Stranger:
                    ViewBag.Username = this.Db.Profiles.Where(profile => profile.UserId == UserId).FirstOrDefault().Name;
                    break;
                case UserStatus.Undefined:
                    return new HttpNotFoundResult("Invalid User");
            }

            if (Request.IsAjaxRequest())
            {
                IEnumerable<KeyValuePair<int, string>> ownedItems = this.Db.Inventory.Where(item => item.Owner.UserId == UserId && !item.Deleted)
                                                                                     .ToList()
                                                                                     .Select( item => new KeyValuePair<int, string>(item.Id, item.Name));
                return Json(ownedItems, JsonRequestBehavior.AllowGet);
            }
            else
            {
                IEnumerable<RentItem> ownedItems = this.Db.Inventory.Where(item => item.Owner.UserId == UserId && !item.Deleted);
                return View(ownedItems);
            }            
        }

        // GET: RentItems/Create
        [ProfileVerification]
        public ActionResult Create()
        {
            return View();
        }

        // POST: RentItems/Create
        [HttpPost]
        [ProfileVerification]
        public ActionResult Create(RentItem item)
        {
            try
            {
                if (ModelState.IsValid && item != null)
                {
                    item.Owner = this.GetCurrentUserProfile();
                    this.Db.Inventory.Add(item);
                    this.Db.SaveChanges();
                    return RedirectToAction("Index");
                }

                return View();
            }
            catch
            {
                return View();
            }
        }

        // GET: RentItems/Edit/5
        [HttpGet]
        [ProfileVerification]
        public ActionResult Edit(int id)
        {
            RentItem item = this.Db.Inventory.Find(id);
            if (item == null)
            {
                return HttpNotFound();
            }

            return View(item);
        }

        // POST: RentItems/Edit/5
        [HttpPost]
        [ProfileVerification]
        public ActionResult SaveEdit(RentItem item)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    this.Db.Entry(item).State = EntityState.Modified;
                    this.Db.SaveChanges();
                    return RedirectToAction("Index");
                }

                return View();
            }
            catch
            {
                return View();
            }
        }

        // GET: RentItems/Delete/5
        [HttpGet]
        [ProfileVerification]
        public ActionResult Delete(int id)
        {
            RentItem item = this.Db.Inventory.Find(id);
            if (item == null)
            {
                return HttpNotFound();
            }

            return View(item);
        }

        // POST: RentItems/Delete/5
        [HttpPost, ActionName("Delete")]
        [ProfileVerification]
        public ActionResult SaveDelete(int id)
        {
            try
            {
                RentItem item = this.Db.Inventory.Find(id);

                if (item == null)
                {
                    return HttpNotFound("The item you are trying to delete doesn't exist!");
                }

                item.Deleted = true;

                this.Db.Entry(item).State = EntityState.Modified;
                this.Db.SaveChanges();
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        [HttpGet]
        public ActionResult GetItemCount(string UserId = null)
        {
            if (string.IsNullOrEmpty(UserId))
            {
                return new HttpNotFoundResult();
            }
            
            int ownedItemCount = this.Db.Inventory.Where(item => item.Owner.UserId == UserId && !item.Deleted).Count();
            return (Content($"Inventory contains {ownedItemCount} item(s)."));
        }
    }
}
