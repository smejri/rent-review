﻿using RentReview.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using System.Web.Helpers;
using System.IO;
using System.Net;
using System.Data.Entity;
using RentReview.CustomFilters;
using RentReview.Extensions;

namespace RentReview.Controllers
{
    public class ProfileController : BaseController
    {
        // GET: Profile
        [ProfileVerification]
        public ActionResult Index(string UserId = null)
        {
            switch (this.VerifyOwnership(UserId))
            {
                case UserStatus.Current:
                    UserProfile currentUserProfile = this.GetCurrentUserProfile();

                    if (Request.IsAjaxRequest())
                    {
                        return PartialView("Profile", currentUserProfile);
                    }

                    return View(currentUserProfile);

                case UserStatus.Stranger:
                    UserProfile requestedUserProfile = this.Db.Profiles.Where(profile => profile.UserId == UserId).FirstOrDefault();

                    if (Request.IsAjaxRequest())
                    {
                        return PartialView("Profile", requestedUserProfile);
                    }

                    return View(requestedUserProfile);

                case UserStatus.Undefined:
                default:
                    return new HttpNotFoundResult("Invalid User");
            }
        }

        [Authorize]
        [ProfileVerification]
        [HttpPost]
        public ActionResult ChangePicture(HttpPostedFileBase file)
        {
            if (file != null)
            {
                string ext = Path.GetExtension(file.FileName).ToLower();

                if (!ext.Equals(".jpeg") && !ext.Equals(".jpg") && !ext.Equals(".png"))
                {
                    return new HttpStatusCodeResult(HttpStatusCode.UnsupportedMediaType, "Image extension not supported.");
                }

                string newUniqueName = $@"{Guid.NewGuid()}{ext}";
                string path = Path.Combine(Server.MapPath("~/images/profilepictures"), newUniqueName);

                WebImage img = new WebImage(file.InputStream);
                //if (img.Width > 200 || img.Height > 200)
                //{
                //    img.Resize(200, 200, true);
                //}

                if (img.Width > img.Height)
                {
                    var leftRightCrop = (img.Width - img.Height) / 2;
                    img.Crop(0, leftRightCrop, 0, leftRightCrop);
                }
                else if (img.Height > img.Width)
                {
                    var topBottomCrop = (img.Height - img.Width) / 2;
                    img.Crop(topBottomCrop, 0, topBottomCrop, 0);
                }

                if (img.Width > 200 || img.Height > 200)
                {
                    img.Resize(200, 200, true);
                }

                img.Save(path);

                UserProfile currentUserProfile = this.GetCurrentUserProfile();
                if (!string.IsNullOrEmpty(currentUserProfile.Image))
                {
                    System.IO.File.Delete(Path.Combine(Server.MapPath("~/images/profilepictures"), currentUserProfile.Image));
                }

                currentUserProfile.Image = newUniqueName;

                this.Db.Entry(this.GetCurrentUserProfile()).State = EntityState.Modified;
                this.Db.SaveChanges();
            }
            // after successfully uploading redirect the user
            return RedirectToAction("Index");
        }

        // GET: Profile/Create
        [Authorize]
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [Authorize]
        public ActionResult Create(UserProfile profile)
        {
            try
            {
                if (ModelState.IsValid && profile != null)
                {
                    profile.UserId = this.CurrentUserIdentity.GetUserId();
                    this.Db.Profiles.Add(profile);
                    this.Db.SaveChanges();
                }

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        [Authorize]
        [HttpGet]
        [ProfileVerification]
        public ActionResult Edit()
        {
            return View(this.GetCurrentUserProfile());
        }

        [HttpPost]
        [Authorize]
        [ProfileVerification]
        public ActionResult Save(UserProfile profile)
        {
            if (ModelState.IsValid)
            {
                this.Db.Entry(profile).State = EntityState.Modified;
                this.Db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View("Edit", profile);
        }

        [HttpGet]
        [OutputCache(CacheProfile = "UserSpecific")]
        public ActionResult GetThumbnail(string UserId = null)
        {
            UserProfile requestedUserProfile = this.Db.Profiles.Find(UserId);

            if (requestedUserProfile != null)
            {
                return PartialView("Profile", requestedUserProfile);
            }
            else
            {
                return new HttpNotFoundResult();
            }
        }

        [HttpGet]
        public ActionResult Search(string searchTerm)
        {
            IEnumerable<UserProfile> foundUsers;

            if (Request.IsAjaxRequest())
            {
                foundUsers = this.Db.Profiles.ToList()
                                .Where(p => p.Name.ToLower().Contains(searchTerm.ToLower()) ||
                                            p.Email.ToLower().Contains(searchTerm.ToLower()) ||
                                            p.MobileNumber.Simplify().Contains(searchTerm.Simplify()));

                return PartialView("UserSearch", foundUsers.AsEnumerable<UserProfile>());
            }
            else
            {
                foundUsers = this.Db.Profiles.ToList()
                                            .Where(p => p.Name.ToLower().Contains(searchTerm.ToLower()) ||
                                                        p.Email.ToLower().Contains(searchTerm.ToLower()) ||
                                                        p.MobileNumber.Simplify().Contains(searchTerm.Simplify()))
                                                        ;
                return View("UserSearch", foundUsers.AsEnumerable<UserProfile>());
            }
        }

        [HttpGet]
        public ActionResult SearchUser(string term)
        {
            var foundUsers = this.Db.Profiles.ToList()
                                            .Where(p => p.Name.ToLower().Contains(term.ToLower()) ||
                                                        p.Email.ToLower().Contains(term.ToLower()) ||
                                                        p.MobileNumber.Simplify().Contains(term.Simplify()) ||
                                                        p.UserId == term)
                                            .Select(p => new { label = p.Name, value = p.UserId, html = this.RenderViewAsString("Profile", p) });

            return Json(foundUsers, JsonRequestBehavior.AllowGet);
        }
        
        public ActionResult GetProfilePicture(string UserId)
        {
            UserProfile requestedProfile = this.Db.Profiles.Find(UserId);
            string image = requestedProfile == null ? null : requestedProfile.Image;

            if (string.IsNullOrEmpty(image))
            {
                return Content("<i class='fa fa-user-circle fa-2x text-light'></i>");
            }
            else
            {
                return Content($"<img src='\\Images\\ProfilePictures\\{image}' class='rounded-circle m-0' style='width:40px; height:40px;'></img>"); //
            }
        }
    }
}