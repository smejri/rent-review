﻿namespace RentReview.Controllers
{
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using System.Web.Mvc;
    using Microsoft.AspNet.Identity;
    using RentReview.CustomFilters;
    using RentReview.Models;

    public class ReviewController : BaseController
    {
        // GET: Review
        public ActionResult Index(string UserId)
        {
            switch (this.VerifyOwnership(UserId))
            {
                case UserStatus.Current:
                    UserId = this.CurrentUserIdentity.GetUserId();
                    break;
                case UserStatus.Stranger:
                    ViewBag.Username = this.Db.Profiles.Where(profile => profile.UserId == UserId).FirstOrDefault().Name;
                    break;
                case UserStatus.Undefined:
                    return new HttpNotFoundResult("Invalid User.");
            }

            return View("Index", this.GetUserReviews(UserId));
        }

        [ProfileVerification]
        public ActionResult Create(int contractId)
        {
            RentContract contract = this.Db.Contracts.Find(contractId);

            if (contract == null)
            {
                return new HttpNotFoundResult($"There is no contract with id {contractId}.");
            }

            RentReview review = new RentReview { Contract = contract };

            return View(review);
        }

        [HttpPost]
        [ProfileVerification]
        public ActionResult Create(RentReview review, int contractId)
        {
            try
            {
                if (ModelState.IsValid && review != null)
                {
                    review.WrittenBy = this.GetCurrentUserProfile();
                    this.Db.Reviews.Add(review);
                    this.Db.SaveChanges();
                }

                RentContract contract = this.Db.Contracts.Find(contractId);

                if (contract != null)
                {
                    review.Contract = contract;

                    if (review.WrittenBy.UserId == contract.Customer.UserId)
                    {
                        contract.CustomerReview = review;
                    }

                    if (review.WrittenBy.UserId == contract.Item.Owner.UserId)
                    {
                        contract.OwnerReview = review;
                    }

                    this.Db.Entry(contract).State = EntityState.Modified;
                    this.Db.SaveChanges();
                }

                return RedirectToAction("Index", "Contract", null);
            }
            catch
            {
                return View();
            }
        }

        public ActionResult GetSummary(string UserId)
        {
            if (string.IsNullOrEmpty(UserId))
            {
                return new HttpUnauthorizedResult("No user ID was is associated with this request.");
            }

            return PartialView("ReviewSummary", this.GetUserReviews(UserId));
        }

        public ActionResult GetItemReviewSummary(int itemId)
        {
            return PartialView("ReviewSummary", this.GetItemReviews(itemId));
        }

        public ActionResult IndexForItemReviews(int itemId)
        {
            ViewBag.IsItem = true;
            ViewBag.Item = this.Db.Inventory.Find(itemId).Name;
            return View("Index", this.GetItemReviews(itemId));
        }

        [NonAction]
        public IEnumerable<RentReview> GetUserReviews(string UserId)
        {
            if (string.IsNullOrEmpty(UserId))
            {
                return null;
            }

            IEnumerable<RentContract> associatedContracts = this.Db.Contracts.Where(contract => contract.Item.Owner.UserId == UserId ||
                                                                                                contract.Customer.UserId == UserId).ToList();

            List<RentReview> incomingReviews = associatedContracts.Where(contract => contract.Customer.UserId == UserId &&
                                                                                            contract.OwnerReview != null &&
                                                                                            contract.CustomerReview != null)
                                                                        .Select(contract => contract.OwnerReview).ToList();

            incomingReviews.AddRange(associatedContracts.Where(contract => contract.Item.Owner.UserId == UserId &&
                                                                                    contract.CustomerReview != null &&
                                                                                    contract.OwnerReview != null)
                                                                .Select(contract => contract.CustomerReview));

            return incomingReviews;
        }

        public IEnumerable<RentReview> GetItemReviews(int itemId)
        {
            IEnumerable<RentReview> itemReviews = this.Db.Contracts.Where(contract => contract.Item.Id == itemId &&
                                                                                    contract.OwnerReview != null &&
                                                                                    contract.CustomerReview != null).
                                                                                    Select(contract => contract.CustomerReview)
                                                                                    .ToList();

            return itemReviews;
        }
    }
}