﻿namespace RentReview.CustomFilters
{
    using Microsoft.AspNet.Identity;
    using RentReview.Models;
    using System;
    using System.Linq;
    using System.Web;
    using System.Web.Mvc;
    using System.Web.Routing;

    public class ProfileVerificationAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            base.OnActionExecuted(filterContext);
        }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            string activeUserId = HttpContext.Current.User.Identity.GetUserId();
            RentReviewDb db = new RentReviewDb();

            UserProfile profile = db.Profiles.Where(p => p.UserId == activeUserId).FirstOrDefault();

            if (activeUserId != null && profile == null)
            {
                filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary { { "Controller", "Profile" }, { "Action", "Create" } });
            }

            base.OnActionExecuting(filterContext);
        }
    }
}