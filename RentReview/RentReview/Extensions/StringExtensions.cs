﻿using System;

namespace RentReview.Extensions
{
    public static class StringExtensions
    {
        public static string Simplify(this String str)
        {
            return str.Replace(" ", string.Empty).Replace("-", string.Empty);
        }
    }
}