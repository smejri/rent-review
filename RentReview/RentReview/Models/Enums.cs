﻿namespace RentReview.Models
{

    public enum PaymentInterval
    {
        OneTime,
        Hourly,
        Daily,
        Monthly,
        Yearly
    }

    public enum Category
    {
        Fabrication,
        IT,
        Photography,
        Accommodation,
        Transportation,
        Other
    }

    public enum UserStatus
    {
        Undefined,
        Current,
        Stranger
    }

    public enum UserRole
    {
        None,
        Owner,
        Customer
    }

    public enum UserResponse
    {
        WaitingReponse,
        Approved,
        Declined
    }

    public enum ContractClassification
    {
        Active,
        Concluded,
        OnHold
    }
}