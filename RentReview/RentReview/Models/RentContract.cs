﻿namespace RentReview.Models
{
    using System;
    using System.ComponentModel.DataAnnotations;

    public class RentContract
    {
        public int Id { get; set; }

        public virtual UserProfile Customer { get; set; }

        public virtual RentItem Item { get; set; }

        [Display(Name = "Start Date")]
        [DataType(DataType.Date)]
        public DateTime StartDate { get; set; }

        [Display(Name = "End Date")]
        [DataType(DataType.Date)]
        public DateTime EndDate { get; set; }

        [Display(Name = "Unit Cost")]
        public decimal UnitCost { get; set; }

        [Display(Name = "Payment Interval")]
        [EnumDataType(typeof(PaymentInterval), ErrorMessage = "Invalid Payment Interval!")]
        public PaymentInterval Interval { get; set; }

        public string Comments { get; set; }

        // [Required(ErrorMessage = "Your full name is required")]
        public virtual RentReview OwnerReview { get; set; }

        // [Required(ErrorMessage = "Your full name is required")]
        public virtual RentReview CustomerReview { get; set; }

        public UserResponse CustomerResponse { get; set; } = UserResponse.WaitingReponse;

        public UserResponse OwnerResponse { get; set; } = UserResponse.WaitingReponse;

        public bool Terminated { get; set; } = false;
    }

    public class UserResponseViewModel
    {
        public UserResponse Response { get; set; } = UserResponse.WaitingReponse;
        public int ContractId { get; set; }
        public string UserId { get; set; }
    }

    public class ContractViewModel
    {
        public RentContract Contract { get; set; }
        public ContractClassification Group
        {
            get
            {
                if (this.Contract.Terminated)
                {
                    return ContractClassification.Concluded;
                }

                if (this.Contract.CustomerResponse == UserResponse.Approved &&
                    this.Contract.OwnerResponse == UserResponse.Approved)
                {
                    return ContractClassification.Active;
                }

                return ContractClassification.OnHold;
            }
        }
    }
}