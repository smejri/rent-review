﻿using System.ComponentModel.DataAnnotations;

namespace RentReview.Models
{
    public class RentItem
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Owner name is required")]
        public string Name { get; set; }
        public string Description { get; set; }

        [Required(ErrorMessage = "Owner name is required")]
        [EnumDataType(typeof(Category), ErrorMessage = "Invalid category!")]
        public Category Category { get; set; }

        [UIHint("UserInput")]
        public virtual UserProfile Owner { get; set; }

        public string City { get; set; }

        public string Country { get; set; }

        public string Url { get; set; }

        public bool Deleted { get; set; } = false;
    }
}