﻿namespace RentReview.Models
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public class RentReview
    {
        public int Id { get; set; }
        public virtual RentContract Contract { get; set; }
        public virtual UserProfile WrittenBy { get; set; }
        public string Pros { get; set; }
        public string Cons { get; set; }
        public string Comments { get; set; }

        [Range(1, 5)]
        [Required]
        public int Rating { get; set; }
    }
}