﻿namespace RentReview.Models
{
    using System.Data.Entity;

    public class RentReviewDb : DbContext
    {
        public RentReviewDb() : base("name=DefaultConnection")
        {
        }

        public DbSet<RentContract> Contracts { get; set; }
        public DbSet<RentReview> Reviews { get; set; }
        public DbSet<UserProfile> Profiles { get; set; }
        public DbSet<RentItem> Inventory { get; set; }
        //public DbSet<ApplicationUser> Users { get; set; }
    }
}