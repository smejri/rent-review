﻿using System.ComponentModel.DataAnnotations;

namespace RentReview.Models
{
    public class UserProfile
    {
        // public int Id { get; set; }

        [Display (Name="Full Name")]
        [Required(ErrorMessage = "Your full name is required")]
        public string Name { get; set; }

        [Display(Name = "Contact Email")]
        [Required(ErrorMessage = "The email address is required")]
        [EmailAddress(ErrorMessage = "Invalid Email Address")]
        public string Email { get; set; }

        [Display(Name = "Mobile Number")]
        [RegularExpression("^\\+?(\\d| |-){6,15}$", ErrorMessage = "Invalid mobile number")]
        public string MobileNumber { get; set; }

        [DataType(DataType.MultilineText)]
        public string Summary { get; set; }

        [Key]
        public string UserId { get; set; }

        public string Image { get; set; }
    }
}