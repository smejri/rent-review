﻿function populateItems(userId, selectedValue) {
    $.ajax({
        dataType: "json",
        url: "/Inventory/Index/" + userId,
        success: function (response) {
            if (response != null) {
                if (response.length == 0) {
                    $("#items").html('<p class="alert-warning">&nbsp;<i class="fa fa-warning"></i> Selected user\'s inventry is empty.</p>');
                }
                else {
                    for (var i = 0; i < response.length; i++) {
                        if (typeof selectedValue !== 'undefined' && selectedValue == response[i].Key) {
                            $("#items").append($("<option selected='selected'>").text(response[i].Value).attr('value', response[i].Key));
                        }
                        else {
                            $("#items").append($("<option>").text(response[i].Value).attr('value', response[i].Key));
                        }
                    }

                    $("#items").wrapInner("<select name='itemId' class='form-control mt-1'></select>");
                }
            }
            else {
                $("#items").html('<p class="alert-danger">&nbsp;<i class="fa fa-warning"></i> Could not load Inventory Items.</p>');
            }
        }
    });
}

function getUserView(userId, targetId) {
    $.ajax({
        dataType: "html",
        url: "/Profile/GetThumbnail/" + userId,
        success: function (response) {
            if (response != null) {
                $('#' + targetId).html(response);
            }
            else {
                $('#' + targetId).add('<p class="alert-danger">&nbsp;<i class="fa fa-warning"></i> Could not find user.</p>');
            }
        }
    });
}

$(document).ready(function () {
    var handleAutoComplete = function ($input, targetId, inputId, loadItemList) {
        var options = {
            source: $input.attr("data-rr-autocomplete"),

            select: function (event, ui) {
                if (typeof targetId !== 'undefined' && typeof targetId !== 'undefined') {
                    // render the selected item in the provided container
                    $('#' + targetId).html(ui.item.html);

                    $('#' + inputId).val(ui.item.value);

                    $input.val('');

                    if (typeof loadItemList !== 'undefined' && loadItemList == true) {
                        populateItems(ui.item.value);
                    }
                }

                return false;
            }
        };

        $input.autocomplete(options);

        $input.autocomplete("instance")._resizeMenu = function () {
            this.menu.element.outerWidth(300);
        };

        $input.autocomplete("instance")._renderItem = function (ul, item) {
            return $("<li>")
                .append(item.html)
                .appendTo(ul);
        };
    };

    var handleFormInput = function () {
        var type = $(this).attr("data-rr-type");

        switch (type) {
            case "Item":
                var defaultItemId = $(this).children("input").attr("value");
                var ownerId;

                if (typeof defaultItemId !== 'undefined') {
                    ownerId = $(this).attr("data-rr-owner");
                }

                // Remove the input used by plain HTML to be replaced with a dropdown.
                $(this).children("input").remove();

                // Add the autocomplete widget input
                var $input = $('<input type="text" placeholder="Select Owner" class="form-control" data-rr-autocomplete="/Profile/SearchUser"/>');
                $(this).append($input);

                // Used to display the selected user.
                $(this).append('<div id="owner" class="col-md-10"></div>');
                if (typeof ownerId !== 'undefined' && ownerId != '') {
                    getUserView(ownerId, 'owner');
                }

                if (typeof ownerId !== 'undefined') {
                    // Used to display the dropdown
                    $(this).append('<div id="items"></div>');
                    populateItems(ownerId, defaultItemId);
                }

                handleAutoComplete($input, 'owner', 'ownerId', true);
                break;

            case "User":
                var defaultUserId = $(this).children("input").attr("value");

                // Hide the input used by plain HTML to be replaced with jQuery widgets
                // The hidden input will be kept up-to-date for the form submission tow work correctly.
                $(this).children("input").attr("type", "hidden");

                // Add the autocomplete widget input
                var $input = $('<input type="text" placeholder="Select Customer" class="form-control" data-rr-autocomplete="/Profile/SearchUser"/>');
                $(this).append($input);

                // Used to display the selected user.
                $(this).append('<div id="customer" class="col-md-10"></div>');
                if (typeof defaultUserId !== 'undefined' && defaultUserId != '') {
                    getUserView(defaultUserId, 'customer');
                }

                handleAutoComplete($input, 'customer', 'customerId');
                break;
            default:
                handleAutoComplete($(this).children('input[name="term"]').first())
        }
    }

    var convertRatingToStars = function () {
        var $input = $(this);
        var targetId = $input.attr("data-rr-rating-target");
        var $target = $(targetId);


        $input.on("change paste keyup", function () {
            var activatedStars = $input.val();
            $target.empty();

            if (activatedStars <= 5 && activatedStars >= 1) {
                var deactivatedStars = 5 - activatedStars;

                for (var i = 0; i < activatedStars; i++) {
                    $target.append('<i class="fa fa-star fa-2x"></i>');
                }

                for (var i = 0; i < deactivatedStars; i++) {
                    $target.append('<i class="fa fa-star-o fa-2x"></i>');
                }

                $target.wrapInner('<p class="text-warning tshadow"></p>')
            }
            else {

                for (var i = 0; i < 5; i++) {
                    $target.append('<i class="fa fa-star-o fa-2x"></i>');
                }

                $target.wrapInner('<p class="text-danger tshadow"></p>')
            }
        });
    }

    //var handleInputList = function () {
    //    var listName = $(this).attr('data-rr-list');
    //    var $target = $($(this).attr('data-rr-list-target'));
    //    var inputClass = typeof $(this).attr('data-rr-list-class') !== 'undefined' ? $(this).attr('data-rr-list-class') : '';

    //    $(this).removeAttr('style');
    //    $(this).siblings('textarea').remove();

    //    var index = 0;

    //    $(this).on('click', function () {
    //        if (listName != '' && $target.length > 0) {
    //            $target.append('<input type="text" class="' + inputClass + '" name="' + listName + '[' + index + ']"/>');
    //            $(this).siblings('input').insertBefore($(this));
    //            index++;
    //        }
    //    });
    //}

    $("div[data-rr-type]").each(handleFormInput);
    $("input[data-rr-rating-target]").each(convertRatingToStars);
    //$("button[data-rr-list]").each(handleInputList);
});