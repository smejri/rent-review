﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(RentReview.Startup))]
namespace RentReview
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
